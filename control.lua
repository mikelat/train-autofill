local util = require("util")

local function add_tcs_fuel_station(records)
    if script.active_mods["Train_Control_Signals"] and settings.global["ml-ta-tcs-fuel"] then
        table.insert(records, {
            station = settings.global["ml-ta-tcs-fuel"].value,
            wait_conditions = {{
                compare_type = "and",
                type = "inactivity",
                ticks = 60
            }}
        })
    end
end

local function add_tcs_depot_station(records)
    if script.active_mods["Train_Control_Signals"] and settings.global["ml-ta-tcs-depot"] then
        table.insert(records, {
            station = settings.global["ml-ta-tcs-depot"].value
        })
    end
end

local function add_inactivity_condition(wait_conditions)
    if settings.global["ml-ta-inactivity"] and settings.global["ml-ta-inactivity"].value > 0 then
        table.insert(wait_conditions, 1, {
            compare_type = "and",
            type = "inactivity",
            ticks = settings.global["ml-ta-inactivity"].value * 60
        })
    end

    return wait_conditions
end

local function matching_filter_slots(train, stations)
    local items = {}
    local provider_search = settings.global["ml-ta-provider-search"].value or ""

    for _, wagon in pairs(train.cargo_wagons) do
        local inventory = wagon.get_inventory(defines.inventory.cargo_wagon)

        if inventory then
            for slot_index = 1, inventory.get_bar() - 1 do
                local filter = inventory.get_filter(slot_index)

                if filter then
                    items[filter] = items[filter] or {
                        count = 0,
                        station = nil
                    }
                    items[filter].count = items[filter].count + 1
                end
            end
        end
    end

    for item, _ in pairs(items) do
        for _, station in pairs(stations) do
            if station:find("[item=" .. item .. "]", 1, true) ~= nil and station:find(provider_search, 1, true) ~= nil then
                items[item].station = station
                break
            end
        end

        if not items[item].station then
            items[item] = nil
        end
    end

    return items
end

local function check_for_warnings(train, player_index)
    for _, carriage in pairs(train.carriages) do
        if carriage.type == "cargo-wagon" then
            local inventory = carriage.get_inventory(defines.inventory.cargo_wagon)
            if inventory then
                for slot_index = 1, inventory.get_bar() - 1 do
                    if not inventory.get_filter(slot_index) then
                        game.players[player_index].print({"ml-ta.warn-slot-mismatch"}, {
                            r = 1,
                            g = 1,
                            b = 0
                        })
                        break
                    end
                end
            end
        end
    end
end

local function total_wagon_slots(train, include_barred)
    local total_slots = 0

    for _, carriage in pairs(train.carriages) do
        if carriage.type == "cargo-wagon" then
            local inventory = carriage.get_inventory(defines.inventory.cargo_wagon)

            if include_barred then
                total_slots = total_slots + #inventory
            else
                total_slots = total_slots + inventory.get_bar() - 1
            end
        end
    end

    return total_slots
end

--- @param e EventData.on_train_schedule_changed
script.on_event(defines.events.on_train_schedule_changed, function(e)
    if not e.player_index then
        return
    end

    global.train_schedules = global.train_schedules or {}
    local schedule = util.table.deepcopy(e.train.schedule)

    if not (schedule and (global.train_schedules[e.train.id] or 0) < #schedule.records) then
        global.train_schedules[e.train.id] = (schedule and #schedule.records) or 0
        return
    end

    local last_station = schedule.records[#schedule.records]

    if last_station and not last_station.wait_conditions then
        local item_counts = matching_filter_slots(e.train, {last_station.station})
        local total_slots = total_wagon_slots(e.train, true)
        local schedule_changed = false

        for item, val in pairs(item_counts) do
            if val.count > 0 then
                schedule_changed = true
                last_station.wait_conditions = add_inactivity_condition(last_station.wait_conditions or {})

                if val.count == total_slots then
                    table.insert(last_station.wait_conditions, {
                        compare_type = "and",
                        type = "full"
                    })
                else
                    table.insert(last_station.wait_conditions, {
                        compare_type = "and",
                        type = "item_count",
                        condition = {
                            first_signal = {
                                type = "item",
                                name = item
                            },
                            constant = game.item_prototypes[item].stack_size * val.count,
                            comparator = settings.global["ml-ta-operator"].value
                        }
                    })
                end
            end
        end

        if schedule_changed then
            check_for_warnings(e.train, e.player_index)
        end
        global.train_schedules[e.train.id] = (schedule and #schedule.records) or 0
        e.train.schedule = schedule

        return
    end

    global.train_schedules[e.train.id] = (schedule and #schedule.records) or 0
end)

--- @param e EventData.on_gui_opened
script.on_event(defines.events.on_gui_opened, function(e)
    global.train_schedules = global.train_schedules or {}

    if not e.player_index then
        return
    end

    if not (e.entity and e.entity.type == "locomotive") then
        return
    end

    local schedule = util.table.deepcopy(e.entity.train.schedule)

    if not (settings.global["ml-ta-autofill-on-empty"].value and (not schedule or not #schedule.records)) then
        global.train_schedules[e.entity.train.id] = (e.entity.train.schedule and #e.entity.train.schedule.records) or 0
        return
    end
    local train_stations = {}
    local schedule_changed = false

    -- prioritize the surface we're on first (so it uses stops on this surface)
    local surfaces = {}
    table.insert(surfaces, e.entity.train.carriages[1].surface)
    for _, surface in pairs(game.surfaces) do
        if surface ~= e.entity.train.carriages[1].surface then
            table.insert(surfaces, surface)
        end
    end

    for _, surface in pairs(surfaces) do
        for _, station in pairs(surface.get_train_stops {
            force = e.entity.train.carriages[1].force
        }) do
            table.insert(train_stations, station.backer_name)
        end
    end

    local item_counts = matching_filter_slots(e.entity.train, train_stations)
    schedule = {
        current = 1,
        records = {}
    }

    local by_station = {}
    local first, _ = next(item_counts)
    if first then
        local singleton = item_counts[first].count == total_wagon_slots(e.entity.train, true)

        for item, val in pairs(item_counts) do
            by_station[val.station] = by_station[val.station] or {}
            by_station[val.station][item] = val
        end

        add_tcs_fuel_station(schedule.records)

        for station, items in pairs(by_station) do
            local wait_conditions = add_inactivity_condition({})
            schedule_changed = true
            if singleton then
                table.insert(wait_conditions, {
                    compare_type = "and",
                    type = "full"
                })
            else
                for item, val in pairs(items) do
                    table.insert(wait_conditions, {
                        compare_type = "and",
                        type = "item_count",
                        condition = {
                            first_signal = {
                                type = "item",
                                name = item
                            },
                            constant = game.item_prototypes[item].stack_size * val.count,
                            comparator = settings.global["ml-ta-operator"].value
                        }
                    })
                end
            end

            add_tcs_depot_station(schedule.records)
            table.insert(schedule.records, {
                station = station,
                wait_conditions = wait_conditions
            })
        end

        add_tcs_depot_station(schedule.records)

        local requester_search = settings.global["ml-ta-requester-search"].value or ""

        if singleton then
            for _, station in pairs(train_stations) do
                if station:find("[item=" .. first .. "]", 1, true) ~= nil and station:find(requester_search, 1, true) ~=
                    nil then
                    schedule_changed = true
                    table.insert(schedule.records, {
                        station = station,
                        wait_conditions = add_inactivity_condition({{
                            compare_type = "and",
                            type = "empty"
                        }})
                    })
                    break
                end
            end
        end

        if schedule_changed then
            check_for_warnings(e.entity.train, e.player_index)
        end

        global.train_schedules[e.entity.train.id] = #schedule.records

        e.entity.train.schedule = schedule
    end
end)
