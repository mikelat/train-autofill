data:extend({
    {
        type = "string-setting",
        name = "ml-ta-provider-search",
        setting_type = "runtime-global",
        default_value = "[item=logistic-chest-passive-provider]",
        allow_blank = true,
        order = "1"
    }, {
        type = "string-setting",
        name = "ml-ta-requester-search",
        setting_type = "runtime-global",
        default_value = "[item=logistic-chest-requester]",
        allow_blank = true,
        order = "2"
    }, {
        type = "bool-setting",
        name = "ml-ta-autofill-on-empty",
        setting_type = "runtime-global",
        default_value = true,
        order = "3"
    }, {
        type = "string-setting",
        name = "ml-ta-operator",
        setting_type = "runtime-global",
        default_value = "=",
        allowed_values = {"=", "≥"},
        order = "4"
    }, {
        type = "int-setting",
        name = "ml-ta-inactivity",
        setting_type = "runtime-global",
        default_value = 1,
        allow_blank = true,
        order = "5"
    }
})

if mods["Train_Control_Signals"] then
    data:extend({
        {
            type = "string-setting",
            name = "ml-ta-tcs-fuel",
            setting_type = "runtime-global",
            default_value = "[virtual-signal=refuel-signal]",
            allow_blank = true,
            order = "7"
        }, {
            type = "string-setting",
            name = "ml-ta-tcs-depot",
            setting_type = "runtime-global",
            default_value = "[virtual-signal=depot-signal]",
            allow_blank = true,
            order = "8"
        }
    })
end
